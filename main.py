import click
import requests
from datetime import timedelta, datetime
import pandas as pd
import pandas_gbq
from google.oauth2.service_account import Credentials
from google.cloud import bigquery

STOPS = {
    "Rennes": "87471003",
}

# Define target table in BQ
target_table = "freddy_sncf.train_retard"
project_id = "ensai-2023-373710"
credential_file = "./ensai-2023-373710-2b23eed594ca.json"
credential = Credentials.from_service_account_file(credential_file)
# Location for BQ job, it needs to match with destination table location
job_location = "us"

def upload_to_bq(dataframe):
    ''' function to upload data to BigQuery '''
    client = bigquery.Client(project='ensai-2023-373710')
    job = client.load_table_from_dataframe(dataframe, 'ensai-2023-373710.freddy_sncf.train_retard')
    job.result()


@click.command()
@click.option('--token')
@click.option('--date', type=click.DateTime(formats=["%Y-%m-%d"]))
@click.option('--ville')
def run(token, date, ville):
    start_date = date.strftime('%Y%m%dT%H%M%S')
    end_date = (date + timedelta(days=1)).strftime("%Y%m%dT%H%M%S")
    url = f"https://api.sncf.com/v1/coverage/sncf/stop_areas/stop_area%3ASNCF%3A{STOPS[ville]}/physical_modes/physical_mode%3ALongDistanceTrain/arrivals?from_datetime={start_date}&until_datetime={end_date}&count=500&"

    response = requests.get(url, headers={'Authorization' : token})
    data = response.json()
    df = pd.DataFrame(data["arrivals"])

    output = []
    for col in df[["display_informations", "stop_date_time"]].columns:
        output.append(pd.json_normalize(df[col]))
    trains = pd.concat(output, axis=1)

    trains["arrival_date_time"] = pd.to_datetime(trains["arrival_date_time"])
    trains["base_arrival_date_time"] = pd.to_datetime(trains["base_arrival_date_time"])
    trains["delay"] = trains["arrival_date_time"] - trains["base_arrival_date_time"]
    trains["delay"] = trains["delay"].apply(lambda x : x.total_seconds())
    trains["is_delayed"] = trains["delay"].astype(int) != 0
    df = trains[["name", "headsign", "trip_short_name", "commercial_mode", "arrival_date_time", "departure_date_time", "base_arrival_date_time", "base_departure_date_time", "delay", "is_delayed"]]
    pandas_gbq.to_gbq(df.astype(str), "freddy_sncf.train_retard", project_id="ensai-2023-373710", credentials=credential,if_exists='replace')

if __name__ == '__main__':
    run()