import requests
from datetime import datetime
import pandas as pd
import os
from dotenv import load_dotenv

load_dotenv()

TOKEN_ID = os.getenv('TOKEN_ID')

headers={'Authorization': TOKEN_ID}

response = requests.get(
    "https://api.sncf.com/v1/coverage/sncf/stop_areas/stop_area%3ASNCF%3A87471003/physical_modes/physical_mode%3ALongDistanceTrain/arrivals?from_datetime=20230105T000000&until_datetime=20230105T235959&count=500&",
    headers=headers)

response_in_json = response.json()

arrivals = response_in_json['arrivals']

list_train_number = []
list_arrival_time_schedule = []
list_arrival_time_real = []
list_delayed = []

for arrival in arrivals:
    list_train_number.append(arrival['display_informations']['trip_short_name'])
    list_arrival_time_schedule.append(datetime.strptime(arrival['stop_date_time']['base_arrival_date_time'],'%Y%m%dT%H%M%S'))
    list_arrival_time_real.append(datetime.strptime(arrival['stop_date_time']['arrival_date_time'],'%Y%m%dT%H%M%S'))
    list_delayed.append(arrival['stop_date_time']['data_freshness'])

dict = {'train_number' : list_train_number, 'scheduled_time' : list_arrival_time_schedule, 'real_time' : list_arrival_time_real, 'delayed' : list_delayed}

df = pd.DataFrame(dict)
df_delayed_trains_only = df[df.delayed == 'realtime']
df_delayed_trains_only['Delayed time'] = df_delayed_trains_only.real_time - df_delayed_trains_only.scheduled_time
