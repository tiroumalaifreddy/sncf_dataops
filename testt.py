import click
import requests
from datetime import timedelta
import pandas as pd
import pandas_gbq
from google.oauth2 import service_account


df = pd.DataFrame(
    {
        "my_string": ["a", "b", "c"],
        "my_int64": [1, 2, 3],
        "my_float64": [4.0, 5.0, 6.0],
        "my_bool1": [True, False, True],
        "my_bool2": [False, True, False],
        "my_dates": pd.date_range("now", periods=3),
    }
)

print(df)
